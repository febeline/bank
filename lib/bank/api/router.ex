defmodule Bank.Api.Router do
  use Plug.Router
  alias Bank.Controllers, as: Controllers
  alias Bank.Api.Plugs.Authenticator
  alias Bank.Api.Validators
  alias Bank.Adapters.Transaction, as: TransactionAdapter
  alias Bank.Adapters.Account, as: AccountAdapter

  plug(Authenticator, ["/api/account/transfer", "/api/account/withdraw", "/api/backoffice/report"])

  plug(Plug.Parsers,
    parsers: [:json],
    json_decoder: {Jason, :decode!, [[keys: :atoms]]}
  )

  plug(:match)
  plug(:dispatch)

  defp render_json(conn, status, data) do
    body = Jason.encode!(data)
    send_resp(conn, status, body)
  end

  ###
  ## handlers
  ###

  defp transaction_handler(conn) do
    resp =
      conn.body_params
      |> TransactionAdapter.wire_to_internal(conn.assigns.account.id)
      |> Controllers.Account.new_transaction()

    case resp do
      {:ok, transaction} ->
        wire_transaction = TransactionAdapter.internal_to_wire(transaction)
        render_json(conn, 201, wire_transaction)

      _ ->
        render_json(conn, 400, %{})
    end
  end

  defp login_handler(conn) do
    resp = Controllers.Auth.login(conn.body_params)

    case resp do
      {:ok, token} ->
        conn = %{conn | resp_headers: [{"Authorization", token}]}
        render_json(conn, 200, %{})

      {:error, _} ->
        render_json(conn, 401, %{})
    end
  end

  defp create_account_handler(conn) do
    case Controllers.Account.create(conn.body_params) do
      {:ok, acc} ->
        wire_account = AccountAdapter.internal_to_wire(acc)
        render_json(conn, 201, wire_account)

      {:error, _} ->
        render_json(conn, 400, %{})
    end
  end

  ###
  ## route definitions
  ###

  post "/api/auth/login" do
    valid_payload? = Validators.check(Validators.login_schema(), conn.body_params)

    case valid_payload? do
      {:ok, _} -> login_handler(conn)
      {:error, err} -> render_json(conn, 400, err)
    end
  end

  post "/api/account" do
    valid_payload? = Validators.check(Validators.account_schema(), conn.body_params)

    case valid_payload? do
      {:ok, _} -> create_account_handler(conn)
      {:error, err} -> render_json(conn, 400, err)
    end
  end

  post "/api/account/transfer" do
    valid_payload? = Validators.check(Validators.transfer_schema(), conn.body_params)

    case valid_payload? do
      {:ok, _} -> transaction_handler(conn)
      {:error, err} -> render_json(conn, 400, err)
    end
  end

  post "/api/account/withdraw" do
    valid_payload? = Validators.check(Validators.withdraw_schema(), conn.body_params)

    case valid_payload? do
      {:ok, _} -> transaction_handler(conn)
      {:error, err} -> render_json(conn, 400, err)
    end
  end

  get "/api/backoffice/report" do
    render_json(conn, 200, Controllers.Backoffice.report())
  end

  get "/api/version" do
    version = Mix.Project.config[:version]
    render_json(conn, 200, %{version: version})
  end

  match _ do
    send_resp(conn, 404, "Oops!")
  end
end
