defmodule Bank.Api.Validators do
  @moduledoc """
  Provide validators to be used in values that comes throght the wire.
  By now it's being used to validate API payloads.
  """

  defp process_errors(errors) do
    errors
    |> Enum.map(fn {:error, field, _, msg} ->
      [field, msg]
    end)
    |> Enum.reduce(%{}, fn [k, v], acc ->
      Map.put(acc, k, v)
    end)
  end

  def transfer_schema() do
    [
      beneficiary_account_id: [presence: true],
      amount: [presence: true, number: [greater_than: 0]]
    ]
  end

  def withdraw_schema() do
    [beneficiary_account_id: [absence: true], amount: [presence: true, number: [greater_than: 0]]]
  end

  def login_schema() do
    [email: [presence: true], password: [presence: true]]
  end

  def account_schema() do
    [email: [presence: true], password: [presence: true, confirmation: true, length: [min: 6]]]
  end

  def check(schema, data) do
    vex_data = Map.put(data, :_vex, schema)

    if Vex.valid?(vex_data) do
      {:ok, data}
    else
      {:error, process_errors(Vex.errors(vex_data))}
    end
  end
end
