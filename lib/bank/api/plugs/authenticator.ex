defmodule Bank.Api.Plugs.Authenticator do
  @moduledoc """
  Plug that checks if access token is provided and valid.
  It only intercepts calls from secure routes.
  """

  import Plug.Conn
  alias Bank.Token

  def init(opts), do: opts

  defp authenticated?(conn) do
    authorization = Plug.Conn.get_req_header(conn, "authorization")

    if Enum.empty?(authorization) do
      {:error, :missing_token}
    else
      authorization
      |> hd
      |> Token.valid?()
    end
  end

  defp authenticate(conn) do
    case authenticated?(conn) do
      {:ok, claims} ->
        conn
        |> assign(:account, %{id: claims["id"], email: claims["email"]})

      {:error, _} ->
        conn
        |> send_resp(401, "")
        |> halt
    end
  end

  def call(conn = %{request_path: request_path}, secure_routes) do
    if request_path in secure_routes do
      authenticate(conn)
    else
      conn
    end
  end
end
