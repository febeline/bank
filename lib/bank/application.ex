defmodule Bank.Application do
  @moduledoc false

  use Application

  def start(_type, _args) do
    port = (System.get_env("PORT") || "8080") |> String.to_integer
    children = [
      {Plug.Cowboy, scheme: :http, plug: Bank.Api.Router, options: [port: port]},
      Bank.Repo
    ]

    opts = [strategy: :one_for_one, name: Bank.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
