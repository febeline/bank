defmodule Bank.Controllers.Account do
  @moduledoc """
  Controller layer for Account related operations
  """

  alias Bank.Database.Schemas.Account
  alias Bank.Database.Repositories.Account, as: AccountRepo
  alias Bank.Database.Repositories.Transaction, as: TransactionRepo

  def create(params) do
    params
    |> Account.of()
    |> AccountRepo.create()
  end

  def new_transaction(transaction) do
    source = AccountRepo.by_id(transaction.source_account_id)
    beneficiary = AccountRepo.by_id(transaction.beneficiary_account_id)
    transaction = Map.put(transaction, :transacted_at, DateTime.utc_now())

    case TransactionRepo.create(source, beneficiary, transaction) do
      {:ok, ch} ->
        # Send email here (ASYNC)
        # One viable option should be the usage of Task.async
        # I don't like controller refering controller so I should use a new layer to talk with
        # external services.
        {:ok, ch.transaction}
      err -> err
    end
  end
end
