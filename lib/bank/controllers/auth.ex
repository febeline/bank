defmodule Bank.Controllers.Auth do
  @moduledoc """
  Controller layer for Authentication related operations
  """

  alias Bank.Database.Repositories.Account, as: AccountRepo
  alias Bank.Token, as: Token

  def login(%{:email => email, :password => password}) do
    account = AccountRepo.by_email(email)

    case Argon2.check_pass(account, password) do
      {:ok, _} ->
        token = Token.gen!(%{id: account.id, email: account.email})
        {:ok, token}

      error ->
        error
    end
  end
end
