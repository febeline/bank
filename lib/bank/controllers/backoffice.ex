defmodule Bank.Controllers.Backoffice do
  @moduledoc """
  Controller layer for backoffice related operations.
  """

  alias Bank.Database.Repositories.Transaction, as: TransactionRepo
  alias Bank.Adapters.Transaction, as: Adapter

  def report() do
    empty_report = %{report: %{}}

    [:day, :month, :year, :total]
    |> Enum.reduce(empty_report, fn period, acc ->
      v =
        period
        |> TransactionRepo.total_amount_transacted()
        |> Adapter.group_to_wire()

      Kernel.put_in(acc, [:report, period], v)
    end)
  end
end
