defmodule Bank.Database.Schemas.Transaction do
  @moduledoc """
  Transaction schema
  """

  use Ecto.Schema
  import Ecto.Changeset
  alias Bank.Database.Schemas.Account

  schema "transactions" do
    field(:amount, :integer)
    field(:transacted_at, :utc_datetime)

    belongs_to(:source_account, Account, foreign_key: :source_account_id)
    belongs_to(:beneficiary_account, Account, foreign_key: :beneficiary_account_id)

    timestamps()
  end

  def changeset(transaction, attrs) do
    fields = [:amount, :source_account_id, :beneficiary_account_id, :transacted_at]

    transaction
    |> cast(attrs, fields)
    |> validate_required(fields)
    |> validate_number(:amount, greater_than: 0)
  end
end
