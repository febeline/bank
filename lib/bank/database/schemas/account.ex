defmodule Bank.Database.Schemas.Account do
  @moduledoc """
  Account schema
  """

  use Ecto.Schema
  import Ecto.Changeset

  schema "accounts" do
    field(:email, :string)
    field(:password_hash, :string)
    field(:balance, :integer, default: 0)

    timestamps()
  end

  def of(data) do
    %{password_hash: hash} = Argon2.add_hash(data.password)
    %{email: data.email, password_hash: hash, balance: 100_000}
  end

  def changeset(account, attrs) do
    account
    |> cast(attrs, [:email, :password_hash, :balance])
    |> validate_format(:email, ~r/^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/)
    |> validate_number(:balance, greater_than_or_equal_to: 0)
    |> unique_constraint(:email)
  end
end
