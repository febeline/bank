defmodule Bank.Database.Repositories.Account do
  @moduledoc """
  Repository layer for account related operations
  """

  alias Bank.Repo
  alias Bank.Database.Schemas.Account

  def by_id(id) do
    Repo.get(Account, id)
  end

  def by_email(email) do
    Repo.get_by(Account, email: email)
  end

  def create(account) do
    %Account{}
    |> Account.changeset(account)
    |> Repo.insert()
  end
end
