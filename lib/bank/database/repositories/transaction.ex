defmodule Bank.Database.Repositories.Transaction do
  @moduledoc """
  Repository layer for transaction related operations
  """

  import Ecto.Query
  alias Ecto.Multi
  alias Bank.Repo
  alias Bank.Database.Schemas.Transaction
  alias Bank.Database.Schemas.Account

  def create(source, beneficiary, transaction) when source != beneficiary do
    source_changeset = Account.changeset(source, %{balance: source.balance - transaction.amount})

    beneficiary_changeset =
      Account.changeset(beneficiary, %{balance: beneficiary.balance + transaction.amount})

    transaction_changeset = Transaction.changeset(%Transaction{}, transaction)

    Multi.new()
    |> Multi.insert(:transaction, transaction_changeset)
    |> Multi.update(:source, source_changeset)
    |> Multi.update(:beneficiary, beneficiary_changeset)
    |> Repo.transaction()
  end

  # if the source and the beneficiary are the same it means a withdraw operation
  def create(source, beneficiary, transaction) when source == beneficiary do
    account_changeset = Account.changeset(source, %{balance: source.balance - transaction.amount})
    transaction_changeset = Transaction.changeset(%Transaction{}, transaction)

    Multi.new()
    |> Multi.insert(:transaction, transaction_changeset)
    |> Multi.update(:account, account_changeset)
    |> Repo.transaction()
  end

  # it's not possible to use interpolated strings with fragment
  # because of that there is some unnecessary duplication while building the query
  def total_amount_transacted(:day) do
    query =
      from(t in Transaction,
        select: [fragment("date_trunc('day', ?) as date", t.transacted_at), sum(t.amount)]
      )

    total_amount_by(query)
  end

  def total_amount_transacted(:month) do
    query =
      from(t in Transaction,
        select: [fragment("date_trunc('month', ?) as date", t.transacted_at), sum(t.amount)]
      )

    total_amount_by(query)
  end

  def total_amount_transacted(:year) do
    query =
      from(t in Transaction,
        select: [fragment("date_trunc('year', ?) as date", t.transacted_at), sum(t.amount)]
      )

    total_amount_by(query)
  end

  def total_amount_transacted(:total) do
    query =
      from(t in Transaction,
        select: [sum(t.amount)]
      )

    query
    |> Repo.one()
    |> List.first()
  end

  defp total_amount_by(query) do
    composed_query =
      from(t in query,
        group_by: fragment("date"),
        order_by: fragment("date")
      )

    Repo.all(composed_query)
  end
end
