defmodule Bank.Adapters.Account do
  @moduledoc """
  Adapters for account schema

  wire: external world
  internal: domain representation
  """

  def internal_to_wire(account) do
    balance = account.balance / 100
    %{id: account.id, email: account.email, balance: balance}
  end
end
