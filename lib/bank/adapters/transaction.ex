defmodule Bank.Adapters.Transaction do
  @moduledoc """
  Adapters for transaction schema

  wire: external world
  internal: domain representation
  """

  defp zero_pad(number, amount \\ 2) do
    number
    |> Integer.to_string()
    |> String.rjust(amount, ?0)
  end

  def wire_to_internal(payload, current_account_id) do
    amount = Kernel.trunc(payload.amount * 100)
    beneficiary = payload[:beneficiary_account_id] || current_account_id
    %{source_account_id: current_account_id, beneficiary_account_id: beneficiary, amount: amount}
  end

  def internal_to_wire(transaction) do
    %{
      id: transaction.id,
      amount: transaction.amount / 100,
      beneficiary_account_id: transaction.beneficiary_account_id,
      transacted_at: transaction.transacted_at
    }
  end

  def group_to_wire(data) when is_list(data) do
    Enum.reduce(
      data,
      %{},
      fn [k, v], acc ->
        date = "#{k.year}-#{k.month |> zero_pad}-#{k.day |> zero_pad}"
        amount = v / 100
        Map.put(acc, date, amount)
      end
    )
  end

  def group_to_wire(amount) when is_integer(amount) do
    amount / 100
  end
end
