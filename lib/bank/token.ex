defmodule Bank.Token do
  @moduledoc """
   Interface Joken functions and extend it.
   This module is used for JWT manipulation.
  """

  use Joken.Config

  def gen!(claims) do
    token = generate_and_sign!(claims)
    "Bearer " <> token
  end

  def valid?(token) do
    case token do
      "Bearer " <> t -> verify_and_validate(t)
      true -> {:error, :missing_token}
    end
  end
end
