FROM bitwalker/alpine-elixir:latest

COPY . .

RUN apk add --no-cache make gcc libc-dev argon2

RUN mix local.hex --force

RUN export MIX_ENV=prod && \
    rm -Rf _build && \
    mix deps.get && \
    mix release

ENTRYPOINT ["_build/prod/rel/bank/bin/bank"]
CMD ["start"]