use Mix.Config

config :bank, Bank.Repo,
  adapter: Ecto.Adapters.Postgres,
  url: System.get_env("DATABASE_URL"),
  pool_size: String.to_integer(System.get_env("POOL_SIZE") || "10"),
  ssl: true

config :joken, default_signer: System.get_env("JWT_SECRET")
