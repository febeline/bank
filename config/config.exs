use Mix.Config

import_config "#{Mix.env()}.exs"

config :bank, ecto_repos: [Bank.Repo]

config :argon2_elixir,
  t_cost: 1,
  m_cost: 8

config :joken, default_signer: System.get_env("JWT_SECRET") || "secret"
