import Config

config :bank, Bank.Repo,
  username: "postgres",
  password: "",
  database: "bank_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox
