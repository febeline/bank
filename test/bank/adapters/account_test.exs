defmodule Bank.Adapters.AccountTest do
  use ExUnit.Case
  alias Bank.Adapters.Account, as: Adapter

  test "On internal to wire" do
    account = %{id: 10, email: "foo@gmail.com", balance: 100_000}
    expected = %{id: 10, email: "foo@gmail.com", balance: 1000}

    assert expected == Adapter.internal_to_wire(account)
  end
end
