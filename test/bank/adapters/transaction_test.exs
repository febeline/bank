defmodule Bank.Adapters.TransactionTest do
  use ExUnit.Case
  alias Bank.Adapters.Transaction, as: Adapter

  test "On wire to internal" do
    source_id = 1
    payload = %{beneficiary_account_id: 2, amount: 125.50}
    expected = %{source_account_id: source_id, beneficiary_account_id: 2, amount: 12_550}

    assert expected == Adapter.wire_to_internal(payload, source_id)
  end

  test "On internal to wire" do
    t = DateTime.utc_now()

    transaction = %{
      id: 1,
      amount: 12_550,
      source_account_id: 1,
      beneficiary_account_id: 2,
      transacted_at: t
    }

    expected = %{id: 1, amount: 125.50, beneficiary_account_id: 2, transacted_at: t}

    assert expected == Adapter.internal_to_wire(transaction)
  end

  test "On group to wire as integer" do
    assert 125.50 == Adapter.group_to_wire(12_550)
  end

  test "On group to wire as list" do
    lst = [[~N[2000-01-01 00:00:00], 300], [~N[2000-01-02 00:00:00], 500]]

    expected = %{"2000-01-01" => 3.0, "2000-01-02" => 5.0}

    assert expected == Adapter.group_to_wire(lst)
  end
end
