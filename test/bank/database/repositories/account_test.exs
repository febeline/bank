defmodule Bank.Database.Repositories.AccountTest do
  use Bank.RepoCase
  alias Bank.Database.Repositories.Account, as: Repository

  @account %{email: "foo@foo.com", password_hash: "123", balance: 100}

  test "On account creation" do
    {:ok, new_account} = Repository.create(@account)

    assert "foo@foo.com" == new_account.email
    assert "123" == new_account.password_hash
    assert 100 == new_account.balance
  end

  test "Email is unique" do
    {:ok, _} = Repository.create(@account)
    {:error, ch} = Repository.create(@account)

    expected = [
      email:
        {"has already been taken", [constraint: :unique, constraint_name: "accounts_email_index"]}
    ]

    assert expected == ch.errors
  end

  test "Balance cannot be negative" do
    invalid_balance_account = Map.put(@account, :balance, -3)
    {:error, ch} = Repository.create(invalid_balance_account)

    expected = [
      balance:
        {"must be greater than or equal to %{number}",
         [validation: :number, kind: :greater_than_or_equal_to, number: 0]}
    ]

    assert expected == ch.errors
  end

  test "Get account by email" do
    {:ok, expected} = Repository.create(@account)
    account = Repository.by_email("foo@foo.com")

    assert expected == account
  end

  test "Get account by id" do
    {:ok, account} = Repository.create(@account)
    account = Repository.by_id(account.id)

    refute is_nil(account)
  end
end
