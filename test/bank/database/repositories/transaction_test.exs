defmodule Bank.Database.Repositories.TransactionTest do
  use Bank.RepoCase
  alias Bank.Database.Repositories.Account, as: AccountRepository
  alias Bank.Database.Repositories.Transaction, as: Repository

  defp new_transaction(source, beneficiary, amount) do
    new_transaction(source, beneficiary, amount, DateTime.utc_now())
  end

  defp new_transaction(source, beneficiary, amount, transacted_at) do
    transaction = %{
      source_account_id: source.id,
      beneficiary_account_id: beneficiary.id,
      transacted_at: transacted_at,
      amount: amount
    }

    Repository.create(source, beneficiary, transaction)
  end

  defp prepare_accounts() do
    {:ok, acc1} =
      AccountRepository.create(%{email: "foo@foo.com", password_hash: "123", balance: 10_000})

    {:ok, acc2} =
      AccountRepository.create(%{email: "bar@bar.com", password_hash: "123", balance: 10_000})

    {acc1, acc2}
  end

  test "On new transaction - transfer" do
    {s, b} = prepare_accounts()
    {:ok, result} = new_transaction(s, b, 5000)

    assert 5000 == result.transaction.amount
    assert 5000 == result.source.balance
    assert 15_000 == result.beneficiary.balance
  end

  test "On new transaction - withdraw" do
    {acc, _} = prepare_accounts()
    {:ok, result} = new_transaction(acc, acc, 1550)

    assert 1550 == result.transaction.amount
    assert 8450 == result.account.balance
  end

  test "Account balances should always be positive" do
    {s, b} = prepare_accounts()
    {:error, :source, ch, _} = new_transaction(s, b, 99_999)

    expected = [
      balance:
        {"must be greater than or equal to %{number}",
         [validation: :number, kind: :greater_than_or_equal_to, number: 0]}
    ]

    assert expected == ch.errors
  end

  test "Total amount transacted by day" do
    {s, b} = prepare_accounts()
    new_transaction(s, b, 1000, ~N[2000-01-01 00:00:00])
    new_transaction(b, s, 5000, ~N[2001-01-01 00:00:00])
    new_transaction(b, s, 3000, ~N[2000-01-02 00:00:00])
    new_transaction(s, b, 1350, ~N[2000-01-02 00:00:00])

    expected = [
      [~N[2000-01-01 00:00:00.000000], 1000],
      [~N[2000-01-02 00:00:00.000000], 4350],
      [~N[2001-01-01 00:00:00.000000], 5000]
    ]

    result = Repository.total_amount_transacted(:day)
    assert expected == result
  end

  test "Total amount transacted by month" do
    {s, b} = prepare_accounts()
    new_transaction(s, b, 1000, ~N[2000-01-01 00:00:00])
    new_transaction(b, s, 2000, ~N[2000-02-03 00:00:00])
    new_transaction(b, s, 1500, ~N[2000-02-10 00:00:00])
    new_transaction(s, b, 1350, ~N[2000-03-07 00:00:00])

    expected = [
      [~N[2000-01-01 00:00:00.000000], 1000],
      [~N[2000-02-01 00:00:00.000000], 3500],
      [~N[2000-03-01 00:00:00.000000], 1350]
    ]

    result = Repository.total_amount_transacted(:month)
    assert expected == result
  end

  test "Total amount transacted by year" do
    {s, b} = prepare_accounts()
    new_transaction(s, b, 1000, ~N[2000-01-01 00:00:00])
    new_transaction(b, s, 2000, ~N[2001-02-03 00:00:00])
    new_transaction(b, s, 3450, ~N[2002-02-10 00:00:00])
    new_transaction(s, b, 1350, ~N[2002-03-07 00:00:00])

    expected = [
      [~N[2000-01-01 00:00:00.000000], 1000],
      [~N[2001-01-01 00:00:00.000000], 2000],
      [~N[2002-01-01 00:00:00.000000], 4800]
    ]

    result = Repository.total_amount_transacted(:year)
    assert expected == result
  end

  test "Total amount transacted" do
    {s, b} = prepare_accounts()
    new_transaction(s, b, 1000, ~N[2000-01-01 00:00:00])
    new_transaction(b, s, 2000, ~N[2001-02-03 00:00:00])

    assert 3000 == Repository.total_amount_transacted(:total)
  end
end
