defmodule Bank.FlowTest do
  use Bank.RepoCase

  @base_url "http://localhost:8080/"

  defp post!(url, payload, headers \\ []) do
    headers = [{"Content-Type", "application/json"}] ++ headers
    response = HTTPoison.post(@base_url <> url, Jason.encode!(payload), headers)

    case response do
      {:ok, v} -> {:ok, %{v | body: Jason.decode!(v.body)}}
      error -> error
    end
  end

  defp get!(url, headers \\ []) do
    headers = [{"Content-Type", "application/json"}] ++ headers
    response = HTTPoison.get(@base_url <> url, headers)

    case response do
      {:ok, v} -> {:ok, %{v | body: Jason.decode!(v.body)}}
      error -> error
    end
  end

  test "Testing the complete application flow" do
    # Account is properly created
    payload = %{email: "foo@foo.com", password: "123456", password_confirmation: "123456"}
    {:ok, response} = post!("api/account", payload)
    account = response.body

    assert 201 == response.status_code
    assert 1000.0 == account["balance"]
    assert "foo@foo.com" == account["email"]

    # Get the Authentication Token (login)
    {:ok, response} = post!("api/auth/login", payload)
    token_header = List.keyfind(response.headers, "Authorization", 0)

    assert 200 == response.status_code
    refute is_nil(token_header)

    # Withdraw operation
    {:ok, response} = post!("api/account/withdraw", %{amount: 200.50}, [token_header])

    assert 201 == response.status_code
    assert 200.5 == response.body["amount"]

    # Creating a beneficiary account
    {:ok, response} =
      post!(
        "api/account",
        %{email: "bar@bar.com", password: "123456", password_confirmation: "123456"}
      )

    beneficiary_id = response.body["id"]

    assert 201 == response.status_code

    # Transfering money between accounts
    payload = %{beneficiary_account_id: beneficiary_id, amount: 150.50}
    {:ok, response} = post!("api/account/transfer", payload, [token_header])

    assert 201 == response.status_code
    assert 150.50 == response.body["amount"]
    assert beneficiary_id == response.body["beneficiary_account_id"]

    # Get backoffice report
    {:ok, response} = get!("api/backoffice/report", [token_header])

    expected_report = %{
      "report" => %{
        "day" => %{"2020-08-04" => 351.0},
        "month" => %{"2020-08-01" => 351.0},
        "year" => %{"2020-01-01" => 351.0},
        "total" => 351.0
      }
    }

    assert 200 == response.status_code
    assert expected_report == response.body
  end
end
