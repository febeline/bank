defmodule Bank.Api.ValidatorsTest do
  use ExUnit.Case
  import Bank.Api.Validators

  test "On Transfer schema validation" do
    data = %{beneficiary_account_id: 1, amount: 100.50}

    assert {:ok, data} == check(transfer_schema(), data)
  end

  test "On Transfer - amount should be positive" do
    data = %{beneficiary_account_id: 1, amount: -10.50}
    expected = {:error, %{amount: "must be a number greater than 0"}}

    assert expected == check(transfer_schema(), data)
  end

  test "On Transfer - beneficiary account should be present" do
    data = %{amount: 10.50}
    expected = {:error, %{beneficiary_account_id: "must be present"}}

    assert expected == check(transfer_schema(), data)
  end

  test "On Withdraw schema validation" do
    data = %{amount: 100.50}

    assert {:ok, data} == check(withdraw_schema(), data)
  end

  test "On Withdraw - beneficiary should not be present" do
    data = %{beneficiary_account_id: 1, amount: 100.50}
    expected = {:error, %{beneficiary_account_id: "must be absent"}}

    assert expected == check(withdraw_schema(), data)
  end

  test "On withdraw - amount should be positive" do
    data = %{beneficiary_account_id: 1, amount: -10.50}
    expected = {:error, %{amount: "must be a number greater than 0"}}

    assert expected == check(transfer_schema(), data)
  end

  test "On Login schema validation" do
    data = %{email: "foo@foo.com", password: "123"}

    assert {:ok, data} == check(login_schema(), data)
  end

  test "On Account schema validation" do
    data = %{email: "foo@foo.com", password: "123456", password_confirmation: "123456"}

    assert {:ok, data} == check(account_schema(), data)
  end

  test "On Account schema - password should have at least 6 characters" do
    data = %{email: "foo@foo.com", password: "123", password_confirmation: "123"}
    expected = {:error, %{password: "must have a length of at least 6"}}
    assert expected == check(account_schema(), data)
  end

  test "On Account schema - password confirmation should match" do
    data = %{email: "foo@foo.com", password: "123456", password_confirmation: "1"}

    expected = {:error, %{password: "must match its confirmation"}}
    assert expected == check(account_schema(), data)
  end
end
