defmodule Bank.RepoCase do
  @moduledoc """
  Case for integration tests.
  Connects to the database provided in the config env file and rollback transactions after
  test finishes.
  """

  use ExUnit.CaseTemplate

  using do
    quote do
      alias Bank.Repo

      import Ecto
      import Ecto.Query
      import Bank.RepoCase
    end
  end

  setup tags do
    :ok = Ecto.Adapters.SQL.Sandbox.checkout(Bank.Repo)

    unless tags[:async] do
      Ecto.Adapters.SQL.Sandbox.mode(Bank.Repo, {:shared, self()})
    end

    :ok
  end
end
