
https://shrouded-crag-88522.herokuapp.com/api/version

## Considerações do projeto

Sobre a organização do projeto, eu montei uma arquitetura hexagonal, baseada em *ports  and  adapters*. Tentando mantê-la o mais enxuta possível dentro do nível de abstração requerido.

Estrutura de pastas:

```
.
├── adapters
├── api
│   ├── plugs
├── controllers
├── database
│   ├── repositories
│   └── schemas

```

## Password hashig

Segundo a especificação apresentada [comeonin](https://github.com/riverrun/comeonin), Argon2 é a função de hashing mais poderosa no momento. Sendo assim, utilizei o projeto [argorn2_elixir](https://github.com/riverrun/argon2_elixir) para realizar o hashing da senha e inseri-lo no banco.

Para que o Argon2 consiga ser compilado será necessário que as seguintes dependências sejam instaladas no host:

- make 
- gcc
- libc-dev

## Autenticação

Para a  autenticação da API foi  utilizado [JWT](https://jwt.io/).

Mantive o padrão da biblioteca [joken](https://github.com/joken-elixir/joken) de 2h de tempo de validade do token. Porém é importante  ressaltar que em  uma  aplicação real o ideal seria  reduzir o tempo para poucos  minutos e utilizar um outro token para revalidar o token expirado.

## Centavos

Para o armazenamento e manipulação dentro do domínio está sento utilizado o valor **inteiro** que representa o montante em centavos.

A vantagem dessa abordagem é não  ter que lidar com arredondamentos e operações com ponto  flutuante.

Porém é importante ressaltar que esta abordagem não funcionará em casos de micro centavos que podem aparecer em operações de aplicação de taxas. (Problema este que não é abordados neste exercício).

## Testes

O projeto  possui tanto teste unitário  como teste de integração.

Os testes de integração exigem conexão com um banco de dados. As informações de conexão estão contidas na configuração do ambiente de teste.

```elixir
# config/test.exs
config :bank, Bank.Repo,
  username: "postgres",
  password: "",
  database: "bank_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox
```

Depois da conexão estar propriamente configurada, basta rodar o comando para criar o banco e os schemas:

```
 MIX_ENV=test mix ecto.create
 MIX_ENV=test mix ecto.migrate
```
Rodando os testes:
```
mix test
```

## API

| VERB | URL | SECURE |
|--|--|--|
| POST | /api/account | no
| POST | /api/auth/login | no
| POST | /api/account/transfer | yes
| POST | /api/account/withdraw | yes
| GET  | /api/backoffice/report | yes
| GET  | /api/version |no 

#### Headers

Para realizar operações autenticadas será necessário um token válido (obtido no processo de login). Para isso basta adicioná-lo no cabeçalho da chamada. e.g.

`"Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImZlbGlwZUBmYmVsaW5lLmNvbSIsImlkIjoxLCJhdWQiOiJKb2tlbiIsImV4cCI6MTU5NTExNjQ2OCwiaWF0IjoxNTk1MTA5MjY4LCJpc3MiOiJKb2tlbiIsImp0aSI6IjJvaGZlcmUzbWNkazlnN2J1MDAwMDAwMiIsIm5iZiI6MTU5NTEwOTI2OH0.hZr4YdJ-cimDVoS1rF4e6y-2-615Odxq1Egldwx6WoY"`

### 1 - Nova Conta

`POST - /api/account`

#### Payload:

```json
{"email": <<string>>,
 "password": <<string>>}
```

#### Response:

| Status |  |
|--|--|
| 201  | Conta criada com sucesso |
| 400  | bad arguments |


### 2 - Autenticação

`POST - /api/auth/login`

#### Payload

```json
{"email": <<string>>,
 "password": <<string>>}
```

#### Response
Em caso de sucesso, o token estará no cabeçalho da resposta em __Authorization__. 

| Status |  |
|--|--|
| 200  | Login realizado com sucesso |
| 401  | Senha e/ou usuário inválido |

### 3 - Tranferências

`[secure] POST - /api/account/transfer`

#### Payload

Aqui deve-se ter em mente que a conta de onde o dinheiro irá sair é a dona do token.

```json
{"beneficiary_account_id": <<number>>,
 "amount": <<number>>} 
```

#### Response

| Status |  |
|--|--|
| 201  | Transferencia realizada com sucesso |
| 400  | Bad arguments - payload inválido |
| 401  | Token de autenticação inválido |

Exemplo de resposta:
```json
{"id":2,
 "amount":253.15,
 "beneficiary_account_id":2,
 "transacted_at":"2020-08-02T23:04:15Z"}
```

### 4 - Saque

`[secure] POST - /api/account/withdraw`

| Status |  |
|--|--|
| 201  | Saque realizado com sucesso |
| 400  | Bad arguments - payload inválido |
| 401  | Token de autenticação inválido |

#### Payload
```json
{"amount": <<number>>} 
```

#### Response
Exemplo de resposta

```json
{
"id":3,
"amount":253.15,  
"beneficiary_account_id":1,    
"transacted_at":"2020-08-04T15:44:08Z"  
}
```

### 5 - Backoffice report

`[secure] GET  - /api/backoffice/report`

| Status |  |
|--|--|
| 200  | Ok |
| 401  | Token de autenticação inválido |

#### Response

Exemplo de resposta:
```json
{
   "report":{
      "day":{
         "2020-8-1":30.25,
         "2020-8-2":253.15,
         "2020-9-10":500.50
      },
      "month":{
         "2020-8-1":283.4,
         "2020-9-1":500.50
      },
      "year":{
         "2020-1-1":783.9
      },
      "total":783.9
   }
}
```

## Docker

1- Construindo a imagem docker: `docker build -t bank .`

2- Rodando a imagem: `docker run bank`

A configuração do ambiente de produção utiliza variáveis de ambiente. Sendo elas:

- PORT
- DATABASE_URL
- POOL_SIZE
- JWT_SECRET

Assim, para que a aplicação consiga subir corretamente as variáveis devem ser configuradas ao executar o comando `docker  run`.

## Setup local

### Dependências

- Postgresql
- Elixir

### DB Setup

Verifique o arquivo `config/dev.exs` para configurar a conexão com o banco.

Rode os comandos para criar o banco e os schemas.

```
mix ecto.create
mix ecto.migrate
```

### Aplicação

- Dependências

`mix deps.get`

- Rodando a aplicação 

`mix run --no-halt`

