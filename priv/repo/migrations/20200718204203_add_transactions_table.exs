defmodule Bank.Repo.Migrations.AddTransactionsTable do
  use Ecto.Migration

  def change do
    create table(:transactions) do
      add :amount, :integer
      add :source_account_id, references(:accounts)
      add :beneficiary_account_id, references(:accounts)
      add :transacted_at, :utc_datetime

      timestamps()
    end
  end
end
