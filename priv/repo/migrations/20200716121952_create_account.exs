defmodule Bank.Repo.Migrations.CreateAccount do
  use Ecto.Migration

  def change do
    create table(:accounts) do
      add :email, :string, null: false
      add :password_hash, :string, null: false
      add :balance, :integer, default: 0

      timestamps()
    end

    create unique_index(:accounts, [:email])
  end
end
